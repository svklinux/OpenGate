#!/bin/bash 
####### Odoo-10c-Install-on-Docker UB18-Mar2019 #######  
OE_USER="odoo"
OE_HOME="/opt/$OE_USER"
OE_HOME_EXT="$OE_HOME/${OE_USER}-server"
OE_PORT="8069"
OE_VERSION="10.0"
OE_SUPERADMIN="admin"
OE_CONFIG="${OE_USER}-server"
WKHTMLTOX_X64=https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.bionic_amd64.deb

#--------------------------------------------------
# Install Dependencies
#--------------------------------------------------
echo -e "\n--- Installing Python 2 & pip --"
sudo apt update -y
sudo apt-get install python python-pip -y

echo -e "\n---- Install tool packages ----"
sudo apt-get install wget git bzr python-pip gdebi-core libxml2-dev libxslt-dev python-dev libsasl2-dev libssl-dev libldap2-dev -y

echo -e "\n---- Install python packages ----"
sudo pip2 install Babel==2.3.4 decorator==4.0.10 docutils==0.12 ebaysdk==2.1.4 feedparser==5.2.1 gevent==1.1.2 greenlet==0.4.10 jcconv==0.2.3 Jinja2==2.8.1 lxml==3.5.0 Mako==1.0.4 MarkupSafe==0.23 mock==2.0.0 ofxparse==0.16 passlib==1.6.5 Pillow==3.4.1 psutil==4.3.1 psycogreen==1.0 psycopg2==2.7.3.1 pydot==1.2.3 pyparsing==2.1.10 pyPdf==1.13 pyserial==3.1.1 Python-Chart==1.39 python-dateutil==2.5.3 python-ldap==2.4.27 python-openid==2.2.5 pytz==2016.7 pyusb==1.0.0 PyYAML==3.12 qrcode==5.3 reportlab==3.3.0 requests==2.20.0 six==1.10.0 suds-jurko==0.6 vatnumber==1.2 vobject==0.9.3 Werkzeug==0.11.11 wsgiref==0.1.2 XlsxWriter==0.9.3 xlwt==1.1.2 xlrd==1.0.0

echo -e "\n--- Install other required packages"
sudo apt-get install -y nodejs npm node-clean-css node-less

### Install wkhtmltopdf ###

wget -q ${WKHTMLTOX_X64}
gdebi --n $(basename ${WKHTMLTOX_X64})
ln -s /usr/local/bin/wkhtmltopdf /usr/bin
ln -s /usr/local/bin/wkhtmltoimage /usr/bin

### Add user for Odoo
adduser --system --quiet --shell=/bin/bash --home=$OE_HOME --gecos 'ODOO' --group $OE_USER
adduser $OE_USER sudo

mkdir /var/log/$OE_USER
chown $OE_USER:$OE_USER /var/log/$OE_USER

#--------------------------------------------------
# Install ODOO
#--------------------------------------------------
echo -e "\e[46m\n==== Installing ODOO Server ====\e[0m"

sudo git clone --depth 1 --branch $OE_VERSION https://www.github.com/odoo/odoo $OE_HOME_EXT/
sleep 5
sudo ls -l $OE_HOME_EXT
read -rsp $'... \e[92mPress any key\e[0m to Continue \e[0mOR \e[31mCTRL+C\e[0m to Exit Installation...\e[0m\n' -n1 key
###
echo -e "\n---- Create custom module directory ----"
sudo su $OE_USER -c "mkdir $OE_HOME/custom"
sudo su $OE_USER -c "mkdir $OE_HOME/custom/addons"

echo -e "\n---- Setting permissions on home folder ----"
sudo chown -R $OE_USER:$OE_USER $OE_HOME/*

#--------------------------------------------------
# Adding Config-File for Odoo
#--------------------------------------------------
echo -e "\e[46m\n==== Create server config file ====\e[0m"

sudo touch /etc/$OE_CONFIG.conf

sudo su root -c "cat > /etc/$OE_CONFIG.conf <<EOF
[options]
db_user = $OE_USER
db_password = False
db_host = False
db_port = False
admin_passwd = $OE_SUPERADMIN
logfile = /var/log/$OE_USER/$OE_CONFIG.log
addons_path=$OE_HOME_EXT/odoo/addons,$OE_HOME_EXT/addons,$OE_HOME/custom/addons
xmlrpc_port = $OE_PORT
EOF
"

sudo chown $OE_USER:$OE_USER /etc/${OE_CONFIG}.conf
sudo chmod 640 /etc/${OE_CONFIG}.conf

echo -e "* Create startup file"
sudo su root -c "echo '#!/bin/sh' >> $OE_HOME_EXT/start.sh"
sudo su root -c "echo 'sudo -u $OE_USER $OE_HOME_EXT/odoo-bin --config=/etc/${OE_CONFIG}.conf' >> $OE_HOME_EXT/start.sh"
sudo chmod 755 $OE_HOME_EXT/start.sh

#--------------------------------------------------
# Adding Start-Script Odoo
#--------------------------------------------------

cat <<EOF > /tmp/odoo-server-init
#!/bin/bash
### BEGIN INIT INFO
# Provides:          odoo-server
# Required-Start:    \$remote_fs \$syslog
# Required-Stop:     \$remote_fs \$syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start odoo daemon at boot time
# Description:       Enable service provided by daemon.
# X-Interactive:     true
### END INIT INFO

. /lib/lsb/init-functions

PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin
DAEMON=$OE_HOME_EXT/odoo-bin
NAME=$OE_CONFIG
DESC=$OE_CONFIG
USER=$OE_USER
CONFIG=/etc/${OE_CONFIG}.conf
PIDFILE=/var/run/\${NAME}.pid
LOGFILE=/var/log/${OE_USER}/${OE_CONFIG}.log
export LOGNAME=\$USER


test -x \$DAEMON || exit 0
set -e

function _start() {
    start-stop-daemon --start --quiet --pidfile \$PIDFILE --chuid \$USER:\$USER --background --make-pidfile --exec \$DAEMON -- --config \$CONFIG --logfile \$LOGFILE
}

function _stop() {
    start-stop-daemon --stop --quiet --pidfile \$PIDFILE --oknodo --retry 3
    rm -f \$PIDFILE
}

function _status() {
    start-stop-daemon --status --quiet --pidfile \$PIDFILE
    return \$?
}

case "\$1" in
        start)
                echo -n "Starting \$DESC: "
                _start
                echo "ok"
                ;;
        stop)
                echo -n "Stopping \$DESC: "
                _stop
                echo "ok"
                ;;
        restart|force-reload)
                echo -n "Restarting \$DESC: "
                _stop
                sleep 1
                _start
                echo "ok"
                ;;
        status)
                echo -n "Status of \$DESC: "
                _status && echo "running" || echo "stopped"
                ;;
        *)
                N=/etc/init.d/\$NAME
                echo "Usage: \$N {start|stop|restart|force-reload|status}" >&2
                exit 1
                ;;
esac
exit 0
EOF

cp /tmp/odoo-server-init /etc/init.d/odoo-server
chmod 755 /etc/init.d/odoo-server
chown root: /etc/init.d/odoo-server
#####################################
