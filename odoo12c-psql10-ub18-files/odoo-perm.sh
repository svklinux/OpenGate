#!/bin/bash
## Setting permission for odoo-erp ##
[ -s /etc/odoo-server.conf ] && chown odoo: /etc/odoo-server.conf
sleep 1
[ -d /var/log/odoo ] && chown -R odoo: /var/log/odoo
sleep 1
[ -d /opt/odoo ] && chown -R odoo: /opt/odoo
sleep 1
exit

