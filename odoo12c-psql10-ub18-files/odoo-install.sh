#!/bin/bash 
####### Odoo-12c-Install-on-Docker #######  
OE_USER="odoo"
OE_HOME="/opt/$OE_USER"
OE_HOME_EXT="$OE_HOME/${OE_USER}-server"
OE_PORT="8069"
OE_VERSION="12.0"
OE_SUPERADMIN="admin"
OE_CONFIG="${OE_USER}-server"
WKHTMLTOX_X64=https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.bionic_amd64.deb

apt update
apt install python3 python3-pip -y
apt install wget git bzr python-pip gdebi-core -y
apt install libxml2-dev libxslt1-dev zlib1g-dev -y
apt install libsasl2-dev libldap2-dev libssl-dev -y
apt install python-pypdf2 python-dateutil python-feedparser python-ldap python-libxslt1 python-lxml python-mako python-openid python-psycopg2 python-pychart python-pydot python-pyparsing python-reportlab python-simplejson python-tz python-vatnumber python-vobject python-webdav python-werkzeug python-xlwt python-yaml python-zsi python-docutils python-psutil python-mock python-unittest2 python-jinja2 python-decorator python-requests python-passlib python-pil -y
sudo pip3 install pypdf2 Babel passlib Werkzeug decorator python-dateutil pyyaml psycopg2 psutil html2text docutils lxml pillow reportlab ninja2 requests gdata XlsxWriter vobject python-openid pyparsing pydot mock mako Jinja2 ebaysdk feedparser xlwt psycogreen suds-jurko pytz pyusb greenlet xlrd chardet libsass
sudo apt install python3-suds -y

apt install node-clean-css -y
apt install node-less -y
apt install python-gevent -y


###Installing Enterprise specific libraries###
sudo pip3 install num2words ofxparse
sudo apt-get install nodejs npm -y
sudo npm install -g less
sudo npm install -g less-plugin-clean-css

### Install wkhtmltopdf ###

wget -q ${WKHTMLTOX_X64}
gdebi --n $(basename ${WKHTMLTOX_X64})
ln -s /usr/local/bin/wkhtmltopdf /usr/bin
ln -s /usr/local/bin/wkhtmltoimage /usr/bin

### Add user for Odoo
adduser --system --quiet --shell=/bin/bash --home=$OE_HOME --gecos 'ODOO' --group $OE_USER
adduser $OE_USER sudo

mkdir /var/log/$OE_USER
chown $OE_USER:$OE_USER /var/log/$OE_USER

#--------------------------------------------------
# Install ODOO
#--------------------------------------------------
echo -e "\e[46m\n==== Installing ODOO Server ====\e[0m"

sudo git clone --depth 1 --branch $OE_VERSION https://www.github.com/odoo/odoo $OE_HOME_EXT/
sleep 5
sudo ls -l $OE_HOME_EXT
read -rsp $'... \e[92mPress any key\e[0m to Continue \e[0mOR \e[31mCTRL+C\e[0m to Exit Installation...\e[0m\n' -n1 key

###

echo -e "\n---- Create custom module directory ----"
sudo su $OE_USER -c "mkdir $OE_HOME/custom"
sudo su $OE_USER -c "mkdir $OE_HOME/custom/addons"

echo -e "\n---- Setting permissions on home folder ----"
sudo chown -R $OE_USER:$OE_USER $OE_HOME/*

#--------------------------------------------------
# Adding Config-File for Odoo
#--------------------------------------------------
echo -e "\e[46m\n==== Create server config file ====\e[0m"

sudo touch /etc/$OE_CONFIG.conf

sudo su root -c "cat > /etc/$OE_CONFIG.conf <<EOF
[options]
db_user = $OE_USER
db_password = False
db_host = False
db_port = False
admin_passwd = $OE_SUPERADMIN
logfile = /var/log/$OE_USER/$OE_CONFIG.log
addons_path=$OE_HOME_EXT/odoo/addons,$OE_HOME_EXT/addons,$OE_HOME/custom/addons
xmlrpc_port = $OE_PORT
EOF
"

sudo chown $OE_USER:$OE_USER /etc/${OE_CONFIG}.conf
sudo chmod 640 /etc/${OE_CONFIG}.conf

echo -e "* Create startup file"
sudo su root -c "echo '#!/bin/sh' >> $OE_HOME_EXT/start.sh"
sudo su root -c "echo 'sudo -u $OE_USER $OE_HOME_EXT/odoo-bin --config=/etc/${OE_CONFIG}.conf' >> $OE_HOME_EXT/start.sh"
sudo chmod 755 $OE_HOME_EXT/start.sh

#--------------------------------------------------
# Adding Start-Script Odoo
#--------------------------------------------------

cat <<EOF > /tmp/odoo-server-init
#!/bin/bash
### BEGIN INIT INFO
# Provides:          odoo-server
# Required-Start:    \$remote_fs \$syslog
# Required-Stop:     \$remote_fs \$syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start odoo daemon at boot time
# Description:       Enable service provided by daemon.
# X-Interactive:     true
### END INIT INFO

. /lib/lsb/init-functions

PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin
DAEMON=$OE_HOME_EXT/odoo-bin
NAME=$OE_CONFIG
DESC=$OE_CONFIG
USER=$OE_USER
CONFIG=/etc/${OE_CONFIG}.conf
PIDFILE=/var/run/\${NAME}.pid
LOGFILE=/var/log/${OE_USER}/${OE_CONFIG}.log
export LOGNAME=\$USER


test -x \$DAEMON || exit 0
set -e

function _start() {
    start-stop-daemon --start --quiet --pidfile \$PIDFILE --chuid \$USER:\$USER --background --make-pidfile --exec \$DAEMON -- --config \$CONFIG --logfile \$LOGFILE
}

function _stop() {
    start-stop-daemon --stop --quiet --pidfile \$PIDFILE --oknodo --retry 3
    rm -f \$PIDFILE
}

function _status() {
    start-stop-daemon --status --quiet --pidfile \$PIDFILE
    return \$?
}

case "\$1" in
        start)
                echo -n "Starting \$DESC: "
                _start
                echo "ok"
                ;;
        stop)
                echo -n "Stopping \$DESC: "
                _stop
                echo "ok"
                ;;
        restart|force-reload)
                echo -n "Restarting \$DESC: "
                _stop
                sleep 1
                _start
                echo "ok"
                ;;
        status)
                echo -n "Status of \$DESC: "
                _status && echo "running" || echo "stopped"
                ;;
        *)
                N=/etc/init.d/\$NAME
                echo "Usage: \$N {start|stop|restart|force-reload|status}" >&2
                exit 1
                ;;
esac
exit 0
EOF

cp /tmp/odoo-server-init /etc/init.d/odoo-server
chmod 755 /etc/init.d/odoo-server
chown root: /etc/init.d/odoo-server
#####################################
